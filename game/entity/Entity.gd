extends CollisionObject2D

export (PackedScene) var explosion

func explode(free = true):
	#print("[Entity] died: ", get_name())
	var explosionInstance = explosion.instance()
	var explosionPos = find_node("explosion-pos")
	if explosionPos:
		explosionInstance.set_pos(get_pos() + explosionPos.get_pos())
	else:
		explosionInstance.set_pos(get_pos())
	get_parent().add_child(explosionInstance)
	explosionInstance.play()
	get_node("/root/GameState").addScore(getScore())
	if free:
		queue_free()

func getScore():
	return 0

func getBullet():
	return null
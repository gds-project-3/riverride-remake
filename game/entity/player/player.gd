extends "res://game/entity/Entity.gd"

const bulletScene = preload("res://game/entity/object/bullet.tscn")

export var normalVelocity = 300
export var maxVelocity = 600
export var minVelocity = 200
export var acceleration = 50

export var bulletVelocity = 1500
export var bulletRange = 1200

export var baseDirection = Vector2(0.0, -1.0)
export var leftDirection = Vector2(-1.4, -1.0)
export var rigthDirection = Vector2(1.4, -1.0)

export(int, 0, 100) var fuelUsage = 5
export(int, 0, 100) var refuelSpeed = 60

var mainGunPos
var jet
var playerState
var leftEngine
var rightEngine
var bullet
var jetActionSound
var jetMoveSound
var jetFuelSound
var jetNormalMoveSoundPlaying = false
var jetSpeedUpMoveSoundPlaying = false
var jetLowFuelSoundPlaying = false

var leftEngineLeftPos = Vector2(-45, -64)
var rightEngineLeftPos = Vector2(50, -64)

var leftEngineNormalPos = Vector2(-55, -64)
var rightEngineNormalPos = Vector2(57, -64)

var leftEngineRightPos = Vector2(-49, -64)
var rightEngineRightPos = Vector2(45, -64)

var dead = false
var shouldFire = false

func _ready():
	set_fixed_process(true)
	set_process_input(true)
	mainGunPos = get_node("main-gun-pos")
	jet = get_node("jet")
	leftEngine = get_node("leftEngine")
	rightEngine = get_node("rightEngine")
	bullet = bulletScene.instance()
	jetMoveSound = get_node("jetMoveSound")
	jetActionSound = get_node("jetActionSound")
	jetFuelSound = get_node("jetFuelSound")

func setPlayerState(playerState):
	self.playerState = playerState

func _input(event):
	processFireGun(event)

func _fixed_process(delta):
	if dead:
		return
	processMove(delta)
	checkCollision()
	if shouldFire:
		shouldFire = false
		fireGun()
	var remainingFuel = playerState.removeFuel(delta * fuelUsage)
	if remainingFuel <= 0:
		print("[Player] No fuel left, crashing!!!")
		explode()
	elif remainingFuel <= 35:
		if !jetLowFuelSoundPlaying:
			jetFuelSound.play("lowFuel")
			jetLowFuelSoundPlaying = true
	else:
		jetFuelSound.stop_all()
		jetLowFuelSoundPlaying = false

func processMove(delta):
	var direction = baseDirection
	if Input.is_action_pressed("player_left"):
		direction = leftDirection
		leftEngine.set_pos(leftEngineLeftPos)
		rightEngine.set_pos(rightEngineLeftPos)
		leftEngine.set_param(Particles2D.PARAM_ORBIT_VELOCITY, 3)
		rightEngine.set_param(Particles2D.PARAM_ORBIT_VELOCITY, 3)
		jet.play("left")
	elif Input.is_action_pressed("player_right"):
		direction = rigthDirection
		leftEngine.set_pos(leftEngineRightPos)
		rightEngine.set_pos(rightEngineRightPos)
		leftEngine.set_param(Particles2D.PARAM_ORBIT_VELOCITY, -3)
		rightEngine.set_param(Particles2D.PARAM_ORBIT_VELOCITY, -3)
		jet.play("right")
	else:
		leftEngine.set_pos(leftEngineNormalPos)
		rightEngine.set_pos(rightEngineNormalPos)
		leftEngine.set_param(Particles2D.PARAM_ORBIT_VELOCITY, 0)
		rightEngine.set_param(Particles2D.PARAM_ORBIT_VELOCITY, 0)
		jet.play("normal")

	var velocity = normalVelocity
	if Input.is_action_pressed("player_speed_up"):
		leftEngine.set_param(Particles2D.PARAM_LINEAR_VELOCITY, 200)
		rightEngine.set_param(Particles2D.PARAM_LINEAR_VELOCITY, 200)
		velocity = maxVelocity
		startJetSpeedUpSound()
	elif Input.is_action_pressed("player_speed_down"):
		leftEngine.set_param(Particles2D.PARAM_LINEAR_VELOCITY, 50)
		rightEngine.set_param(Particles2D.PARAM_LINEAR_VELOCITY, 50)
		velocity = minVelocity
		startJetNormalSound()
	else:
		leftEngine.set_param(Particles2D.PARAM_LINEAR_VELOCITY, 100)
		rightEngine.set_param(Particles2D.PARAM_LINEAR_VELOCITY, 100)
		startJetNormalSound()

	move(velocity * direction * delta)

func processFireGun(event):
	if event.is_action_pressed("player_fire_gun"):
		if !bullet.isFlying():
			shouldFire = true
		else:
			print("[Player] Can't fire, bullet is flying")

func fireGun():
	jetActionSound.play("fire")
	spawnBullet()

func checkCollision():
	if is_colliding():
		var collider = get_collider()
		print("[Player] Collision with: ", collider.get_name())
		if collider.has_method("explode"):
			collider.explode()
		explode()

func spawnBullet():
	print("[Player] Fire, bullet will be added")
	bullet.init(self, bulletVelocity, baseDirection, bulletRange, get_pos() + mainGunPos.get_pos())
	get_parent().add_child(bullet)

func refuel(delta):
	var fuelToAdd = refuelSpeed * delta
	playerState.addFuel(fuelToAdd)

func addScore(score):
	playerState.addFuel(score)

func explode():
	if !dead:
		dead = true
		print("[Player] died, postion: ", get_pos())
		jetMoveSound.stop_all()
		jetActionSound.play("explosion")
		.explode(false)
		jet.stop()
		jet.hide()
		leftEngine.hide();
		rightEngine.hide();
		playerState.die()

func isDead():
	return dead

func startJetNormalSound():
	if !jetNormalMoveSoundPlaying:
		jetSpeedUpMoveSoundPlaying = false
		jetNormalMoveSoundPlaying = true
		jetMoveSound.play("normal")

func startJetSpeedUpSound():
	if !jetSpeedUpMoveSoundPlaying:
		jetSpeedUpMoveSoundPlaying = true
		jetNormalMoveSoundPlaying = false
		jetMoveSound.play("speedUp")

func _on_spottedEnemyArea_body_enter( body ):
	print("[Player] Spotted: ", body.get_name())
	if body.is_in_group("enemy"):
		body.startActingRandomly()

func getBullet():
	return bullet

extends "res://game/entity/enemy/enemy.gd"

var tankTrack
var moving = false

func _ready():
	tankTrack = get_node("tank_track")

func moveEnemy(delta):
	var movement = .moveEnemy(delta)
	if !abs(movement.x) > 0.01:
		var trackRect = tankTrack.get_region_rect().grow_individual(0, 0, abs(moveDirection.x) * velocity * delta, 0)
		tankTrack.set_region_rect(trackRect)
		moving = true
	else:
		moving = false

func fireGun(delta):
	if !moving:
		.fireGun(delta)
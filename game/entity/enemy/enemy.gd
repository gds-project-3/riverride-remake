extends "res://game/entity/Entity.gd"

const bulletScene = preload("res://game/entity/object/bullet.tscn")

enum MoveType { River, Fly, Land }
export var willMove = false
export var willFire = false
export var velocity = 300
export var moveDirection = Vector2(-1, 0)
export var killScore = 0
export var maxActDelay = 3

export var bulletVelocity = 800
export var bulletRange = 1900
export var artillery = false

var bulletDirection = Vector2(0, 1)

var rotation = 0
var shouldAct = false
var actDelay = 0
var bullet
var mainGunPos
var moveAnimation

export(int, "River", "Fly", "Land") var moveType = MoveType.River

func _ready():
	set_fixed_process(true)
	if willMove:
		moveAnimation = get_node("moveAnimation")
	if willFire:
		bullet = bulletScene.instance()
		mainGunPos = get_node("mainGunPos")

func _fixed_process(delta):
	checkCollision()
	processActDelay(delta)
	processMove(delta)
	processFire(delta)

func processActDelay(delta):
	if shouldAct && (willMove || willFire):
		if actDelay <= maxActDelay:
			actDelay = actDelay + delta * randf() * 10
			#print("[Enemy] ", get_name(), " waiting to act: ", maxActDelay - actDelay)

func processMove(delta):
	if willMove:
		if shouldAct && actDelay >= maxActDelay:
			playMoveAnimation()
			moveEnemy(delta)

func processFire(delta):
	if willFire:
		if shouldAct && actDelay >= maxActDelay:
			fireGun(delta)

func fireGun(delta):
	if !bullet.isFlying():
		spawnBullet()

func spawnBullet():
	var bulletPos = mainGunPos.get_pos()
	#print("[Enemy] ", get_name(), " fire bullet at: ", bulletPos, ", mainGunPos: ", mainGunPos.get_pos())
	bullet.init(null, bulletVelocity, moveDirection, bulletRange, bulletPos, artillery)
	bullet.set_rot(bulletDirection.angle_to(moveDirection))
	if MoveType.Fly == moveType:
		bullet.set_collision_mask_bit(0, false)
		bullet.set_layer_mask_bit(0, false)
	#print("[Enemy] adding bullet to: ", get_parent().get_name())
	add_child(bullet)

func playMoveAnimation():
	if moveAnimation:
		moveAnimation.show()
		moveAnimation.play()

func moveEnemy(delta):
	#print("[Enemy] ", get_name(), ", moveDirection=", moveDirection, ", rotation=", get_rotd(), ", pos=", get_pos())
	if moveType == MoveType.Fly:
		if get_pos().x < -100:
			set_pos(Vector2(2020, get_pos().y))
		elif get_pos().x > 2020:
			set_pos(Vector2(-100, get_pos().y))

	var movement = move(velocity * moveDirection * delta)
	if rotation > 0.01:
		rotate(rotation)
		rotation = 0
	return movement

func checkCollision():
	if is_colliding():
		if MoveType.River == moveType:
			checkCollisionInWater()
		elif MoveType.Land == moveType:
			checkCollisionOnLand()

func checkCollisionInWater():
	var collider = get_collider()
	if !collider.is_in_group("player"):
		moveDirection = moveDirection * -1
		rotation = PI
	else:
		explode(true)
		collider.explode()

func checkCollisionOnLand():
	#print("[Enemy] Stopped moving: ", self)
	willMove = false

func getScore():
	return killScore

func startActingRandomly():
	shouldAct = true

func getBullet():
	return bullet

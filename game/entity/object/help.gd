extends "res://game/entity/Entity.gd"

var gameState

func _ready():
	gameState = get_node("/root/GameState")

func _on_help_body_enter( body ):
	if body.is_in_group("player"):
		print("[Help] Player is on help")
		gameState.addScore(1000)
	if body.is_in_group("bullet"):
		print("[Help] Bullet killed this poor soul")
		gameState.addScore(200)
		body.explode()
		explode()
	get_parent().remove_child(self)
	queue_free()
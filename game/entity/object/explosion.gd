extends Node2D

func _ready():
	var sound = get_node("explosionSound")
	if sound:
		sound.play("explosion")

func onLiveTimer():
	#print("Explosion expired")
	queue_free()
extends "res://game/entity/Entity.gd"

var levelState
export var killScore = 0

func setLevelState(levelState):
	self.levelState = levelState

func explode():
	if levelState:
		levelState.nextLevel()
	.explode()

func getScore():
	return killScore
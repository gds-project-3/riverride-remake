extends "res://game/entity/Entity.gd"

var player
var refuelJet
export var killScore = 0

func _fixed_process(delta):
	if player:
		player.refuel(delta)

func _on_fuel_body_enter( body ):
	if body.is_in_group("player"):
		print("[Fuel] Player is on fuel")
		player = body
		set_fixed_process(true)
		refuelJet = get_node("refuelJet").play("refuel")
	if body.is_in_group("bullet"):
		print("[Fuel] Bullet hit fuel")
		explode()
		get_parent().remove_child(self)
		body.explode()

func _on_fuel_body_exit( body ):
	if body.is_in_group("player"):
		player = null
		set_fixed_process(false)

func getScore():
	return killScore
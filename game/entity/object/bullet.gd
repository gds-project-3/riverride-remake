extends KinematicBody2D

const explosionAir = preload("res://game/entity/object/explosion-bullet-air.tscn")
const explosionWater = preload("res://game/entity/object/explosion-bullet-water.tscn")

var velocity
var source
var direction
var bulletRange
var flying
var startPos
var artillery

func init(source, velocity, direction, bulletRange, position, artillery = false):
	self.source = source
	self.velocity = velocity
	self.direction = direction
	self.bulletRange = bulletRange
	self.flying = false
	self.artillery = artillery
	set_pos(position)
	startPos = position
	
func _ready():
	set_fixed_process(true)
	flying = true
	print("[Bullet] Ready")

func _fixed_process(delta):
	move(velocity * direction * delta)
	if source && weakref(source).get_ref():
		trackSource()

	checkCollision()

	if isMaxRange():
		print("[Bullet] Max range, dying")
		explode(artillery)

func checkCollision():
	if is_colliding():
		var collider = get_collider()
		print("[Bullet] Collision with: ", collider.get_name())
		if collider.is_in_group("entity"):
			if self == collider.getBullet():
				return
			collider.explode()
		explode()

func explode(artillery = false):
	if get_parent():
		flying = false
		var explosionInstance
		if artillery:
			explosionInstance = explosionWater.instance()
		else:
			explosionInstance = explosionAir.instance()
		explosionInstance.set_pos(get_pos())
		get_parent().add_child(explosionInstance)
		explosionInstance.play()
		get_parent().remove_child(self)

func isFlying():
	return flying

func trackSource():
	var newPos = get_pos()
	newPos.x = source.get_pos().x
	set_pos(newPos)

func isMaxRange():
	return get_pos().distance_to(startPos) > bulletRange
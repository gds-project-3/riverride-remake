extends Node

const maxFuel = 100

var numberOfLifes = 4
var score = 0
export var newLifeThreshold = 10000
var newLifeScoreCounter = newLifeThreshold
var fuel = maxFuel

var playerNode
var currentLevel
var currentState = PlayerState.Playing
var gameState

enum PlayerState { Playing, Dying }
var dyingTimer

func _init(numberOfLifes, gameState):
	self.numberOfLifes = numberOfLifes
	self.gameState = gameState
	dyingTimer = Timer.new()
	dyingTimer.connect("timeout", self, "onDyingTimeout")
	dyingTimer.set_wait_time(1.0)

func _ready():
	printLog()
	add_child(dyingTimer)

func _exit_tree():
	if playerNode && weakref(playerNode).get_ref():
		playerNode.queue_free()

func changeLevel(newLevel):
	currentLevel = newLevel
	if playerNode && weakref(playerNode).get_ref():
		playerNode.get_parent().remove_child(playerNode)
		currentLevel.add_child(playerNode)
	print("[PlayerState] changing level to: ", currentLevel.get_name())

func spawnNewPlayerNode():
	print("[PlayerState] Spawning player node")
	var currentLevel = gameState.levelState.currentLevel;
	var spawnPoint = currentLevel.get_node("player-spawn")
	playerNode = preload("entity/player/player.tscn").instance()
	playerNode.set_pos(spawnPoint.get_pos() + currentLevel.get_pos())
	print("[PlayerState] Player spawned at pos: ", playerNode.get_pos())
	playerNode.setPlayerState(self)
	gameState.levelState.add_child(playerNode)
	currentState = PlayerState.Playing
	return playerNode

func printLog():
	print("[PlayerState] state=", currentState, ", lifes=", numberOfLifes, ", score=", score,
		", fuel=", fuel)

func play():
	spawnNewPlayerNode()

func die():
	numberOfLifes = numberOfLifes - 1
	print("[PlayerState] Killing player...")
	printLog()
	currentState = PlayerState.Dying
	dyingTimer.start()
	fuel = maxFuel
	gameState.setFuelPercent(fuel / maxFuel)

func onDyingTimeout():
	print("[PlayerState] Freeing player node")
	playerNode.queue_free()
	dyingTimer.stop()
	if numberOfLifes > 0:
		gameState.restartLevel()
	else:
		gameState.endGame()

func getNumberOfLifes():
	return numberOfLifes

func getScore():
	return score

func addScore(scoreToAdd):
	score = score + scoreToAdd
	if score >= newLifeScoreCounter:
		numberOfLifes = numberOfLifes + 1
		newLifeScoreCounter = newLifeScoreCounter + newLifeThreshold
		gameState.setNumberOfLifes(numberOfLifes)
		printLog()

func addFuel(fuelToAdd):
	var newFuel = min(fuel + fuelToAdd, maxFuel)
	logFuel(fuel, newFuel)
	fuel = newFuel
	gameState.setFuelPercent(fuel / maxFuel)
	return fuel

func removeFuel(usedFuel):
	var newFuel = fuel - usedFuel
	logFuel(fuel, newFuel)
	fuel = newFuel
	gameState.setFuelPercent(fuel / maxFuel)
	return fuel

func logFuel(fuel, newFuel):
	var largerFuel = max(fuel, newFuel)
	var smallerFuel = min(fuel, newFuel)
	logFuelLevel(smallerFuel, largerFuel, 10)
	logFuelLevel(smallerFuel, largerFuel, 25)
	logFuelLevel(smallerFuel, largerFuel, 50)
	logFuelLevel(smallerFuel, largerFuel, 75)
	logFuelLevel(smallerFuel, largerFuel, 99)

func logFuelLevel(smallerFuel, largerFuel, fuelLevel):
	if smallerFuel <= fuelLevel and largerFuel > fuelLevel:
		print("[PlayerState] Fuel at %d" % fuelLevel)	
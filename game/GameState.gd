extends Node

const PlayerState = preload("PlayerState.gd")
var menuMusic = preload("res://game/gui/menuMusic.tscn").instance()

var playerState
var levelState

enum State { Menu, Play }
var state = State.Menu

func _ready():
	set_process_input(true)
	set_pause_mode(Node.PAUSE_MODE_PROCESS)
	add_child(menuMusic)
	menuMusic.play("menu")

func launchNewGame(prevScene):
	state = State.Play
	menuMusic.stop_all()
	levelState = preload("res://game/level/LevelState.tscn").instance()
	get_node("/root").add_child(levelState)
	prevScene.queue_free()
	createNewGame()

func showLeaderboard(prevScene):
	playMusic()
	state = State.Menu
	var leaderboard = preload("res://game/gui/Leaderboard.tscn").instance()
	get_node("/root").add_child(leaderboard)
	prevScene.queue_free()
	return leaderboard

func showCredits(prevScene):
	playMusic()
	state = State.Menu
	var credits = preload("res://game/gui/Credits.tscn").instance()
	get_node("/root").add_child(credits)
	prevScene.queue_free()
	return credits

func showMainMenu(prevScene):
	playMusic()
	state = State.Menu
	var mainMenu = preload("res://game/gui/MainMenu.tscn").instance()
	get_node("/root").add_child(mainMenu)
	prevScene.queue_free()

func playMusic():
	if state != State.Menu:
		menuMusic.play("menu")

func showLeaderboardAndScore(prevScene, name, score):
	showLeaderboard(prevScene).addScore(name, score)

func _input(event):
	if state == State.Play:
		if Input.is_action_pressed("pause"):
			if get_tree().is_paused():
				unPauseGame()
			else:
				pauseGame()
		if Input.is_action_pressed("player_fire_gun") && get_tree().is_paused():
			unPauseGame()
		if Input.is_action_pressed("ui_cancel"):
			endGame()
	else:
		if Input.is_action_pressed("ui_cancel"):
			get_tree().quit()

func pauseGame():
	get_tree().set_pause(true)
	levelState.showHelp()

func unPauseGame():
	get_tree().set_pause(false)
	levelState.hideHelp()

func restartLevel():
	var currentLevel = levelState.restartLevel(self, playerState)
	playerState.play()
	state = State.Play
	pauseGame()

func createNewGame():
	playerState = PlayerState.new(4, self)
	add_child(playerState)
	restartLevel()

func setNumberOfLifes(numberOfLifes):
	levelState.setNumberOfLifes(numberOfLifes)

func endGame():
	pauseGame()
	levelState.hideHelp()
	levelState.endGame(playerState)
	playMusic()
	state = State.Menu
	playerState.queue_free()
	print("GAME OVER")

func addScore(scoreToAdd):
	playerState.addScore(scoreToAdd)
	levelState.setScore(playerState.getScore())

func setFuelPercent(fuelPercent):
	levelState.setFuelPercent(fuelPercent)
extends CanvasLayer

var scores = [0, 0, 0, 0, 0, 0, 0, 0, 0]
var scoresLabel
var ok
var gameState

func _ready():
	scoresLabel = get_node("scores")
	gameState = get_node("/root/GameState")
	ok = get_node("ok")
	ok.grab_focus()
	loadHighScores()
	fillHighScoreLabel()

func loadHighScores():
	var highscoreFile = File.new()
	if highscoreFile.file_exists("user://highscore.save"):
		highscoreFile.open("user://highscore.save", File.READ)
		var currentline = highscoreFile.get_line()
		scores = []
		while (!highscoreFile.eof_reached()):
			scores.append(currentline.to_int())
			currentline = highscoreFile.get_line()
		highscoreFile.close()

func saveHighScores():
	var highscoreFile = File.new()
	highscoreFile.open("user://highscore.save", File.WRITE)
	for s in scores:
    	highscoreFile.store_line("%d" % s)
	highscoreFile.close()

func addScore(name, score):
	for i in range(scores.size()):
		if score > scores[i]:
			for j in range(scores.size() - 1, i, -1):
				scores[j] = scores[j - 1]
			scores[i] = score
			break
	saveHighScores()
	fillHighScoreLabel()

func fillHighScoreLabel():
	var newScoresLabelText = ""
	for i in range(scores.size()):
		var score = "%d. %010d" % [i + 1, scores[i]]
		newScoresLabelText = newScoresLabelText + "\n" + score
	print("[Leaderboard] scores=", scores)
	print("[Leaderboard] scoresAsLabel=", newScoresLabelText)
	scoresLabel.set_text(newScoresLabelText)

func _on_ok_pressed():
	gameState.showMainMenu(self)

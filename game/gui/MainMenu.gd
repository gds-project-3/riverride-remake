extends CanvasLayer

var gameState
var current_scene
var loader
var startButton
var menuSound

func _ready():
	set_pause_mode(Node.PAUSE_MODE_PROCESS)
	gameState = get_node("/root/GameState")
	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() -1)
	startButton = get_node("start")
	startButton.grab_focus()
	get_node("logo-anim").play()

func _on_start_pressed():
	gameState.launchNewGame(current_scene)

func _on_exit_pressed():
	current_scene.queue_free()
	get_tree().quit()

func _on_highscore_pressed():
	gameState.showLeaderboard(current_scene)

func _on_credits_pressed():
	gameState.showCredits(current_scene)

extends CanvasLayer

var ok
var gameState

func _ready():
	ok = get_node("ok")
	ok.grab_focus()
	gameState = get_node("/root/GameState")

func _on_ok_pressed():
	gameState.showMainMenu(self)
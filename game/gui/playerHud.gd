extends Node2D

var numberOfLifes
var fuelLevel
var lifeNumber
var fuel

func _ready():
	lifeNumber = get_node("lifeNumber")
	fuel = get_node("fuel")


func setNumberOfLifes(numberOfLifes):
	self.numberOfLifes = numberOfLifes
	lifeNumber.set_text("%d" % numberOfLifes)	
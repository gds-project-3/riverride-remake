extends Node2D

var currentLevel
var nextLevel
var prevLevel
var prevPrevLevel
export var currentLevelNumber = 1
export var fuelAnimationLastFrameIndex = 21
var levelSize = 5440
var fpsLabel

var levelSound
var gameOverSound
var scoreLabel
var bridgeLabel
var fuelAnimation
var lifeNumber
var lowFuel
var gameOverOverlay
var scoreSummary
var ok
var gameState
var lastScore = 0
var music
var help
var name

func _ready():
	set_process(true)
	fpsLabel = get_node("hud/fps")
	levelSound = get_node("levelSound")
	gameOverSound = get_node("gameOverSound")
	scoreLabel = get_node("hud/playerHud/score")
	bridgeLabel = get_node("hud/playerHud/bridge")
	fuelAnimation = get_node("hud/playerHud/fuel")
	lifeNumber = get_node("hud/playerHud/lifeNumber")
	lowFuel = get_node("hud/lowFuel")
	gameOverOverlay = get_node("hud/gameOverOverlay")
	gameOverOverlay.hide()
	scoreSummary = get_node("hud/gameOverOverlay/scoreSummary")
	ok = get_node("hud/gameOverOverlay/ok")
	music = get_node("music")
	help = get_node("hud/help")
	name = get_node("hud/gameOverOverlay/name")

func _process(delta):
	fpsLabel.set_text("FPS %.0f" % OS.get_frames_per_second())

func restartLevel(gameState, playerState):
	self.gameState = gameState
	print("[LevelState] restartLevel, currentLevel=", currentLevelNumber)
	var currentLevelPos = Vector2(0, 0)
	currentLevel = tryFreeLevel(currentLevel)
	nextLevel = tryFreeLevel(nextLevel)

	currentLevel = loadLevelScene(currentLevelNumber).instance()
	currentLevel.set_pos(currentLevelPos)
	registerInLevelBridge(currentLevel)

	add_child(currentLevel)

	tryLoadNextLevel()
	tryFreePrevLevel()
	tryFreePrevPrevLevel()

	levelSound.play("start-game")
	
	setNumberOfLifes(playerState.getNumberOfLifes())
	bridgeLabel.set_text("%04d" % currentLevelNumber)
	return currentLevel

func showHelp():
	print("[LevelState] showing help")
	help.show()

func hideHelp():
	print("[LevelState] hiding help")
	help.hide()

func nextLevel():
	print("[LevelState] Next level!")
	if nextLevel:
		prevPrevLevel = prevLevel
		prevLevel = currentLevel
		currentLevel = nextLevel
		currentLevelNumber = currentLevelNumber + 1
		bridgeLabel.set_text("%04d" % currentLevelNumber)
		nextLevel = null
		tryLoadNextLevel()
		tryFreePrevPrevLevel()
	else:
		print("[LevelState] No next level, game finished!")

func tryLoadNextLevel():
	if !nextLevel:
		var nextLevelScene = loadLevelScene(currentLevelNumber + 1)
		if nextLevelScene:
			nextLevel = nextLevelScene.instance()
			print("[LevelState] Adding next level: ", nextLevel.get_name())
			nextLevel.set_pos(currentLevel.get_pos() + Vector2(0, -levelSize))
			registerInLevelBridge(nextLevel)
			add_child(nextLevel)
		else:
			print("[LevelState] No level with number: ", currentLevelNumber + 1)

func registerInLevelBridge(level):
	var bridge = level.find_node("bridge")
	print("[LevelState] Bridge ", bridge, " on level ", level.get_name())
	if bridge:
		bridge.setLevelState(self)

func tryFreeLevel(level):
	if level:
		print("[LevelState] Freeing level: ", level.get_name())
		level.queue_free()
	return null

func tryFreePrevLevel():
	prevLevel = tryFreeLevel(prevLevel)

func tryFreePrevPrevLevel():
	prevPrevLevel = tryFreeLevel(prevPrevLevel)

func loadLevelScene(levelNumber):
	var levelPath = "res://game/level/level%03d.tscn" % levelNumber
	print("[LevelState] Loading level: ", levelPath)
	return load(levelPath)

func setScore(score):
	scoreLabel.set_text("%010d" % score)
	scoreSummary.set_text("%010d" % score)
	lastScore = score

func setFuelPercent(fuelPercent):
	var frame = fuelAnimationLastFrameIndex - round(fuelPercent * fuelAnimationLastFrameIndex)
	fuelAnimation.set_frame(frame)
	if fuelPercent < 0.35:
		lowFuel.show()
	else:
		lowFuel.hide()

func endGame(playerState):
	gameOverOverlay.show()
	setNumberOfLifes(playerState.getNumberOfLifes())
	ok.grab_focus()
	gameOverSound.play("gameOver")
	music.stop()

func setNumberOfLifes(numberOfLifes):
	lifeNumber.set_text("%d" % numberOfLifes)

func _on_ok_pressed():
	gameState.showLeaderboardAndScore(self, name, lastScore)
